<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>2-step form</title>
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700&amp;subset=cyrillic" rel="stylesheet">
	<link rel="stylesheet" href="css/style.css"> 
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
</head>
<body>
	<h2>Income Protection For...</h2>
	<h3>Income Protection Enquiry</h3>
	<fieldset>
		<legend>
			<a href="/">Your Phone</a>
		</legend>

		<form class="form-register" action="step2.php" method="post">
			
			<p>Some text to explain why phone number is need</p>

			<div class="form-register-item">
				<label for="form-register-item-first-phone" class="form-register-item-label">Phone</label>
				<input type="text" id="form-register-item-first-phone" name="form-register-item-first-phone" class="form-register-item input" pattern="^[0-9]+$" placeholder="" size="35" maxlength="128" required="">
				<div class="form-register-item-error-data">
					<span class="invalid">Enter your phone number(example: 
612123456)</span>
				</div>
			</div>

			<button type="submit" class="form-register-button-submit">Submit your data</button>

		</form>
	</fieldset>
	<script type="text/javascript" src = '../js/script.js'></script>
	
</body>
</html>