<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>2-step form</title>
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700&amp;subset=cyrillic" rel="stylesheet">
	<link rel="stylesheet" href="css/style.css"> 
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
</head>
<body>
	<h2>Income Protection For...</h2>
	<h3>Income Protection Enquiry</h3>
	<fieldset>
		<legend>
			<a href="/">Your Details</a>
		</legend>

		<form class="form-register" action="step1.php" method="post">
			
			<div class="form-register-item">
				<label for="form-register-item-occupation" class="form-register-item-label">Occupation<span class="form-register-item-optional-text"> - Optional</span></label>
				<input type="text" id="form-register-item-occupation" name="form-register-item-occupation" class="form-register-item input" pattern="^[a-zA-Z0-9 ]+$" placeholder="" size="35" maxlength="128">
				<div class="form-register-item-error-data">
					<span class="invalid">Some text to explain what data this field need</span>
				</div>
			</div>

			<div class="form-register-item">
				<label for="form-register-item-annual-income" class="form-register-item-label">Annual Income<span class="form-register-item-optional-text"> - Optional</span></label>
				<span>$</span>
				<input type="text" id="form-register-item-annual-income" name="form-register-item-annual-income" class="form-register-item input" pattern="[0-9]{0,3}" placeholder="" size="33" maxlength="128">
				<div class="form-register-item-error-data">
					<span class="invalid">Some text to explain what data this field need</span>
				</div>
			</div>

			<div class="form-register-item">
				<label for="form-register-item-first-name" class="form-register-item-label">First Name</label>
				<input type="text" id="form-register-item-first-name" name="form-register-item-first-name" class="form-register-item input" pattern="^[a-zA-Z ]{3,}$" placeholder=" " size="35" maxlength="128" required>
				<div class="form-register-item-error-data">
					<span class="invalid">Enter please your name (latin letters, minimum 3 symbols)</span>
				</div>
			</div>

			<div class="form-register-item">
				<label for="form-register-item-last-name" class="form-register-item-label">Last Name</label>
				<input type="text" id="form-register-item-last-name" name="form-register-item-last-name" class="form-register-item input" pattern="^[a-zA-Z ]{3,}$" placeholder="" size="35" maxlength="128" required>
				<div class="form-register-item-error-data">
					<span class="invalid">Enter please your name (latin letters, minimum 3 symbols)</span>
				</div>
			</div>

			<div class="form-register-item">
				<label for="form-register-item-email" class="form-register-item-label">Email</label>
				<input type="email" id="form-register-item-email" name="form-register-item-email" class="form-register-item input" pattern="^[\w\d][\w\d\.-_]+@[\w]{1,}\.[\w\d]+$" placeholder="" size="35" maxlength="128" required>
				<div class="form-register-item-error-data">
					<span class="invalid">Enter please your email (example: user@uu.ua)</span>
				</div>
			</div>

			<div class="form-register-item">
				<label for="form-register-item-age" class="form-register-item-label">Age</label>
				<input type="number" id="form-register-item-age" name="form-register-item-age" class="form-register-item input" pattern="^[0-9]{1,2}$" placeholder="" required>
				<div class="form-register-item-error-data">
					<span class="invalid">Enter please your name (only numbers)</span>
				</div>
			</div>

			<div class="form-register-item">
				<label for="form-register-item-gender" class="form-register-item-label">Gender</label>

				<input type="radio" id="form-register-item-gender-male" name="form-register-item-gender" value="male">
				<label for="form-register-item-gender-male" class="form-register-item-radio">Male</label>

				<input type="radio" id="form-register-item-gender-female" name="form-register-item-gender" value="female">
				<label for="form-register-item-gender-female" class="form-register-item-radio">Female</label>

				<div class="form-register-item-error-data">
					<span class="invalid">Minimum</span>
				</div>
			</div>

			<div class="form-register-item">
				<label for="form-register-item-smoking-status" class="form-register-item-label">Smoking Status</label>
				<input type="radio" id="form-register-item-smoking-status-yes" name="form-register-item-smoking-status" value="yes"><label for="form-register-item-smoking-status-yes" class="form-register-item-radio">Smoker</label>
				<input type="radio" id="form-register-item-smoking-status-no" name="form-register-item-smoking-status" value="no"><label for="form-register-item-smoking-status-no" class="form-register-item-radio">Non-smoker</label>
				<div class="form-register-item-error-data">
					<span class="invalid">Minimum</span>
				</div>
			</div>

			<div class="form-register-item">
				<input type="checkbox" id="form-register-item-remember" name="form-register-item-remember" checked="checked">
				<label for="form-register-item-remember" class="form-register-item-label">Remember me</label>
			</div>

			<input type="hidden" name="form-register-item-ip">

			<button type="submit" class="form-register-button-submit">Get Quotes &#187;</button>

		</form>
	</fieldset>

	<script type="text/javascript" src = '../js/script.js'></script>
	
</body>
</html>