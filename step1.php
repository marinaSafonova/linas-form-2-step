<?php
header('Content-Type: text/html; charset=utf-8');
include('connectDB.php');

$occupation = isset($_POST['form-register-item-occupation'])? trim(strip_tags($_POST['form-register-item-occupation'])) : '';
$annual = isset($_POST['form-register-item-annual-income'])? trim(strip_tags($_POST['form-register-item-annual-income'])) : '';
$firstName = trim(strip_tags($_POST['form-register-item-first-name']));
$lastName = trim(strip_tags($_POST['form-register-item-last-name']));
$email = trim(strip_tags($_POST['form-register-item-email']));
$age = trim(strip_tags($_POST['form-register-item-age']));
$gender = trim(strip_tags($_POST['form-register-item-gender']));
$smoker = trim(strip_tags($_POST['form-register-item-smoking-status']));
$ip = $_SERVER['REMOTE_ADDR'];

$sqlQuery = 'INSERT INTO userStep (firstname, lastname, email, age, gender, smoker, occupation, annual, ip) VALUES ("'.$firstName.'", "'.$lastName.'", "'.$email.'", "'.$age.'", "'.$gender.'", "'.$smoker.'", "'.$occupation.'", "'.$annual.'", "'.$ip.'")';
$result = mysqli_query($conn, $sqlQuery);
if ($result)  
{
	session_start();
	$_SESSION["name"] = $firstName;
	$_SESSION["id"] = $conn->insert_id;
	header('Location: formStep2.php');

}

?>