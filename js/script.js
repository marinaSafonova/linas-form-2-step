$('input').blur(function(){
	var inputTextValue = $(this).val();

	var regExpInput = new RegExp(this.getAttribute('pattern'));
	console.log(inputTextValue, regExpInput);
	console.log(regExpInput.test(inputTextValue));
	if (regExpInput.test(inputTextValue)) {
		$(this).css('border-color', '').css('background', 'url(../images/icon-success30.png) right center no-repeat');
	}
	else {
		$(this).css('border-color', '#d22d2d').css('background', 'url(../images/icon-danger30.png) right center no-repeat');
	}

});