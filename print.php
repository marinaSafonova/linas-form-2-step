<!DOCTYPE html>
<head>
	<meta charset="UTF-8">
	<title>All records</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" class="href">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.min.css" class="href">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css" />
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>

</head>
<body>
	<div class="page-container">
		<table cellpadding="7" cellspacing="7" class="table table-hover table-responsive" id="res_tab">
			<thead>
				<th>Id</th>
				<th>First Name</th>
				<th>Last Name</th>
				<th>Email</th>
				<th>Phone</th>
				<th>Age</th>
				<th>Gender</th>
				<th>Smoker</th>
				<th>Remember</th>
				<th>Occupation</th>
				<th>Annual Income</th>
				<th>IP</th>

			</thead>
			<tbody>
				<?php
				include('connectDB.php');
				$sqlQuery = "SELECT * FROM userStep";
				$result = mysqli_query($conn, $sqlQuery);
				if($result) {
					while ($row = mysqli_fetch_assoc ($result)) {
						// print_r($row);
						?>
						<tr>
							<td><?=$row['id']?></td>
							<td><?=$row['firstname']?></td>
							<td><?=$row['lastname']?></td>
							<td><?=$row['email']?></td>
							<td><?=$row['phone']?></td>
							<td><?=$row['age']?></td>
							<td><?=$row['gender']?></td>
							<td><?=$row['smoker']?></td>
							<td><?=$row['remember']?></td>
							<td><?=$row['occupation']?></td>
							<td><?=$row['annual']?></td>
							<td><?=$row['ip']?></td>
						</tr>
						<?php
					}
				}
				else if(!$row = mysql_fetch_array($result))
					echo '<tr>
				<td colspan="5">No Data</td>
			</tr> ';
			?>  
		</tbody> 
	</table>
</div>
</body>
</html>