<?php
	session_start();
	header('Content-Type: text/html; charset=utf-8');
	
	if (empty($_SESSION['id']))
	{
		include('formStep1.php');
	}
	else
	{
		include('formStep2.php');
	}
?>